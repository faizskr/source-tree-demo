({
	loadOptions : function(component, event, helper) {
		var whereClause = 'where product__c =';
        var valueClause = component.get("v.recordId");
		helper.fetchPickValues(component, event,"Generic_Name__c","Product_Generic_Name__c",whereClause,valueClause);
        helper.fetchPickValues(component, event,"Generic_Name__c,Therapy_Area__c,Name","Product_Brand__c",whereClause,valueClause);
	},
    
    getTherapyArea : function(component, event, helper) {
        var whereClause = 'where Product_Generic_Name__r.Name =';
        var valueClause = component.find("compName").get("v.value");
        component.set('v.checkGeneric', true);
		helper.fetchPickValues(component, event,"Name","Product_Therapy_Area__c",whereClause,valueClause);
    },
    
    getBrands : function(component, event, helper) {
        var selectedField = 'Text_4__c';
        
        var objName = 'Setting_Item__c';
        var selectedVal = component.find("therapyArea").get("v.value");
        var selectedVal2 = component.find("compName").get("v.value");
        var myMap = component.get('v.therapyMap');        
        var selectedValFinal = myMap.get(selectedVal);
        component.set('v.genNameFinal',selectedValFinal);                
        component.set('v.checkIndication', true);            
		var andClause = ' WHERE Setting__r.Name =\'Dove Compounds\' AND Text_2__c =\''+selectedValFinal+'\' AND Text_1__c = \''+selectedVal2+'\'';   
        component.set('v.checkTherapy', true);
        helper.initialLoad(component, event, helper,selectedField,andClause,objName);
    },
    
    saveBrands: function(component, event, helper) {
      //  component.get('v.selectedIndication');
      //  component.find("therapyArea").get("v.value");
        helper.saveProdBrand(component, event,component.get('v.recordId'),component.get('v.selectedBrands'),component.find("therapyArea").get("v.value"));
        var whereClause = 'where product__c =';
        var valueClause = component.get("v.recordId");		
        helper.fetchPickValues(component, event,"Generic_Name__c,Therapy_Area__c,Name","Product_Brand__c",whereClause,valueClause);
        component.set('v.checkGeneric', false);
        component.set('v.checkTherapy', false);
        component.set('v.checkIndication', false);
        component.find("compName").set("v.value",'-- None --');
    },
    
    recordBrands : function(component, event, helper) {
     //   console.log(component.find("Indication").get("v.value"));
        component.set('v.selectedBrands',component.find("brands").get("v.value"));
    },
    
    handleRowAction : function(component, event, helper) {
   		var row = event.getParam('row');        
        console.log('@row@'+JSON.stringify(row.Id));
        helper.deleteProdBrand(component,event,row.Id);
        var whereClause = 'where product__c =';
        var valueClause = component.get("v.recordId");		
        helper.fetchPickValues(component, event,"Generic_Name__c,Therapy_Area__c,Name","Product_Brand__c",whereClause,valueClause);
    }
})